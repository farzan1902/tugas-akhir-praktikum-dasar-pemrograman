Nama	: Dilla Nur Fadhilla
NIM	: 1207050029
Kelas	: Teknik Informatika B

Soal Latihan No. 6 BAB 1
6. Di manakah letak kesalahan algoritma menjalankan sepeda motor berikut ini: 
ALGORITMA menjalankan sepeda motor: 
1. Hidupkan starter 
2. Masukkan kunci kontak 
3. Tekan gigi 1 
4. Perbesar gas 
5. Jalan

Jawaban:
Kesalahan algoritma diatas ada pada nomor 2 yang seharusnya berada pada nomor 1, sehingga ALGORITMA menjalankan sepeda motor:
1. Masukkan kunci kontak
2. Hidupkan starter
3. Tekan gigi 1
4. Perbesar gas
5. Jalan 


