#include <iostream>
using namespace std;

int main ()
{
	float luas,panjang,lebar;
	cout<<"MENGHITUNG LUAS PERSEGI PANJANG"<<endl;
	cout<<"-------------------------------"<<endl;
	cout<<"Masukan Panjang\t\t: ";
	cin>>panjang;
	cout<<"Masukan Lebar\t\t: ";
	cin>>lebar;
	luas=panjang*lebar;
	cout<<"-------------------------------"<<endl;
	cout<<"Luas Persegi Panjang\t: "<<luas<<endl;
	return 0;
}
