Nama 	: Frinaldi Muhammad Syauqi
NIM	: 1207050041
Kelas	: Teknik Informatika-B
Kelompok: 3

Soal Latihan Bab 11:
8.	Buatlah fungsi lower yang mengubah huruf kecil menjadi huruf besar (kapital)

Jawaban:
#include <iostream> //Program konversi huruf kecil ke besar
using namespace std;

char* lower(char teks[30]) {
    for(int i=0;i<teks[i];i++)
   		teks[i]=toupper(teks[i]);
    return teks;
}

main() {
    char kata[30];
    cout<<"Input huruf kecil : ";
    cin.getline(kata,sizeof(kata));
    cout<<"Hasil konversi    : "<<lower(kata);
}