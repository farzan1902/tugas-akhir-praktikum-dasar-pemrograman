#include <iostream>	
#include <math.h>			//buat manggil fungsi abs(), gunanya buat nilai mutlak

struct jam {				//membentuk tipe data baru
	int HH,MM,SS;
};

jam durasi(jam waktu1, jam waktu2);

main(){
	jam W1,W2,W3;

	printf("\n\n\t=============================================");
	printf("\n\t| Keterangan :\t\t\t\t    |\n\t| Format penulisan jam -> (HH:MM:SS).\t    |");
	printf("\n\t=============================================");

	printf("\n\n\tMasukkan waktu pertama : ");
	scanf("%ld:%ld:%ld",&W1.HH,&W1.MM,&W1.SS);

	printf("\n\tMasukkan waktu kedua   : ");
	scanf("%ld:%ld:%ld",&W2.HH,&W2.MM,&W2.SS);

    W3 = durasi(W1, W2);
	printf("\n\tSelisih waktunya selama %u jam, %u menit, dan %u detik.", W3.HH, W3.MM, W3.SS);
	
	std::cin.get();
}

jam durasi(jam waktu1, jam waktu2){
	long J1,J2,J3;
	jam waktu3;
	J1 = (waktu1.SS)+(waktu1.MM*60)+(waktu1.HH*3600);
	J2 = (waktu2.SS)+(waktu2.MM*60)+(waktu2.HH*3600);
	J3 = abs(J2-J1);	
    waktu3.HH = J3/3600;
	waktu3.MM = (J3%3600)/60;
    waktu3.SS = (J3%3600)%60;
    
	return waktu3;
}		
