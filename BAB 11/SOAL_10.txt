Nama 	: Frinaldi Muhammad Syauqi
NIM	: 1207050041
Kelas	: Teknik Informatika-B
Kelompok: 3

Soal Latihan Bab 11:
10. Buatlah fungsi phytagoras yang menerima tiga buah bilangan bulat a, b, c dan menentukan apakah ketiga bilangan tersebut merupakan phytagoras. Contoh: a = 3, b = 4, dan c = 5 adalah tripel phytagoras karena 5^2=3^2+4^2.

Jawaban:
#include <iostream> //Program Menentukan phytagoras
#include <cmath>
using namespace std;

int phytagoras(int a,int b,int c) {
	if(pow(a,2)==pow(b,2)+pow(c,2)) {
		return 1;
	}
	else if(pow(b,2)==pow(a,2)+pow(c,2)) {
		return 1;
	}
	else if(pow(c,2)==pow(b,2)+pow(a,2)) {
		return 1;
	}
	else {
		return 0;
	}
}

main() {
	int a,b,c;
	cout << "Masukkan bilangan a : ";cin >> a;
	cout << "Masukkan bilangan b : ";cin >> b;
	cout << "Masukkan bilangan c : ";cin >> c;
	cout << endl;
	if(phytagoras(a,b,c)==1) {
		cout << "Ketiga bilangan merupakan phytagoras !!";
	}
	else {
		cout << "Ketiga bilangan BUKAN phytagoras";
	}
}
